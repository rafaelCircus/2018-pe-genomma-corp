<?php

    use Jenssegers\Blade\Blade;

    class App {

        private static $singleton;
        public static $router;
        private static $match;

        private function __construct() {
        }

        public static function pre() {
            if (!self::$singleton instanceof self) {
                self::$singleton = new self;
                self::setup();
            }
            return self::$singleton;
        }

        public static function setup() {
            self::$router = new AltoRouter();
            self::$router->setBasePath("");

            $GLOBALS['blade'] = new Blade(['./resources/views'], './cache');
            self::set_ActiveRecord();
        }

        public static function set_ActiveRecord() {
          ActiveRecord\Config::initialize(function($cfg) {
            $cfg->set_model_directory('app/models');
            $cfg->set_connections(array(
                'development' => 'sqlite://app/data/database.db'));
         	});
        }

        public function addRoute($method, $route, $target, $name) {
            self::$router->map($method, $route, $target, $name);
        }

        public function start() {
            self::$match = self::$router->match();
            $this->executeRoute();
        }

        private function _404() {
            header('HTTP/1.0 404 Not Found', true, 404);
            include 'public/404.html';
        }

        private function executeRoute() {
            if (self::$match) {
                list($controller, $method) = explode("#", self::$match['target']);
                $controller = 'App\\Controllers\\' . $controller;

                if (is_callable(array($controller, $method))) {
                    $object = new $controller();
                    call_user_func_array(array($object, $method), array(self::$match['params']));
                } else {
                    self::_404();
                }
            } else {
                self::_404();
            }
        }
    }
