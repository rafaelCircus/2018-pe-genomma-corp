<?php

	require 'vendor/autoload.php';
	// require_once 'vendor/activerecord/ActiveRecord.php';

	$App = App::pre();

	$App->addRoute('GET', '/', 'StaticsController#index', 'home');
	$App->addRoute('GET', '/politicas', 'StaticsController#politics', 'politicas');
	$App->addRoute('GET', '/avisoLegal', 'StaticsController#terms', 'aviso');

	$App->start();
