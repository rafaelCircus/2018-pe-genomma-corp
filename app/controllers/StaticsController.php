<?php
	namespace App\Controllers;

	use App\Models\User;


	class StaticsController {

		function index() {
			view('statics.index');
		}

		function politics() {
			view('statics.politics');
		}

		function terms() {
			view('statics.terms');
		}

	}
