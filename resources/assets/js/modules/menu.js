import {
    TweenMax
} from "gsap";

class Menu {

    constructor() {
        this.menuBtn = "#toggle-menu";
        this.tl = null;
        this.isMobile = false;
        this.init();
    }

    init() {
        this.isMobile = $("body").hasClass('mobile');
        this.addEvents();
    }

    addEvents() {
        $(document).on("click", '.menu-link', () => {
            this.hide();
        });
        $(document).on("click", this.menuBtn, () => {
            if ($(this.menuBtn).hasClass('active')) this.hide();
            else this.show();
        });
    }

    show() {
        $("#main-nav-container, " + this.menuBtn).addClass('active');
        TweenMax.killAll();
        if (this.isMobile) {
            TweenMax.to("#main-nav-content", 0.5, {
                ease: Power2.easeOut,
                display: "block",
                opacity: 1,
                y: 0
            });
        } else {
            TweenMax.to("#main-nav-content", 0.5, {
                ease: Power2.easeOut,
                height: 46,
                top: 21
            });
        }
        TweenMax.staggerTo("#main-nav-content li", 0.25, {
            ease: Power2.easeOut,
            y: 0,
            rotationX: "0deg",
            opacity: 1
        }, 0.075);
    }

    hide() {
        TweenMax.killAll();
        $("#main-nav-container, " + this.menuBtn).removeClass('active');
        if (this.isMobile) {
            TweenMax.to("#main-nav-content", 0.35, {
                ease: Power2.easeOut,
                delay: 0.1,
                opacity: 0,
                display: "none",
                y: 50
            });
        } else {
            TweenMax.to("#main-nav-content", 0.5, {
                ease: Power2.easeOut,
                delay: 0.185,
                height: 0,
                top: 32
            });
        }
        TweenMax.staggerTo("#main-nav-content li", 0.75, {
            ease: Power2.easeOut,
            y: 10,
            rotationX: "50deg",
            opacity: 0
        }, 0.075);
    }
}

module.exports = Menu;
