import {
    TimelineLite
} from "gsap";

class Preloader {
    constructor(args) {
        this.id = args.id || "preloader";
        this.container = document.getElementById(this.id);
        this.init();
    }
    init() {
        document.body.classList.add("loading");
        // window.addEventListener("load", this.hide.bind(this), false);
    }
    hide() {
        document.body.classList.remove("loading");
        TweenMax.to(this.container, 0.5, {
            ease: Power1.ease,
            delay: 0.5,
            y: "-100%",
            display: "hide"
        });
    }
}

module.exports = Preloader;
