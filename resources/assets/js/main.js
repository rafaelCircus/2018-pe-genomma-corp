import {
    TimelineLite
} from "gsap";

require("./lib/owl.carousel");

/* instances */
// var $ = require("jquery");
var Preloader = require("./modules/preloader");
var Menu = require("./modules/menu");

var App = {
    preloader: null,
    log: null,
    menu: null,
    isMobile: null,
    init: function() {
        $('html').removeClass('no-js');
        App.isMobile = isMobileDevice() ? 'mobile' : 'desktop';
        $('body').addClass(App.isMobile);
        App.menu = new Menu();
        App.preloader = new Preloader("preloader");
        App.addEvents();
        App.scrollTo(0);

        if(App.isMobile === 'mobile')
          $(".mobile_slider").addClass('owl-carousel owl-theme').owlCarousel({
            items: 1
          });
    },
    load: function() {
        App.preloader.hide();
	      TweenLite.delayedCall(0.1, function(){
		        $(".item-animable").each(function(index, el) {
	             showItem(el, (index * 0.05)+0.2);
	          });
	      });
    },
    addEvents: function() {
        /* Prevent Default in all links with href === '#' */
        $(document).on("click", "a[href='#']", function(event) {
            event.preventDefault();
        });
        /** Good practice, add toggle class only in 'anchor' or 'button' */
        /*Scroll to*/
        $(document).on("click", ".nav-content .menu-link", function(e) {
            e.preventDefault();
            var _target = $(this).attr('href');
            var _top = $(_target).offset().top;
            App.scrollTo(_top);
        });

        $(document).on("click", ".country", function() {
          var _target = $(this).data("target");
          /**/
          var $target = _target === undefined ? $(this) : $(_target);
          $target.toggleClass('active');
        });

        $(".countryM").click(function(event) {
           $("#country-selectorM").toggle();
           var _target = $(this).data("target");
           var $target = _target === undefined ? $(this) : $(_target);
           $target.toggleClass('active');
        });
    },
    scrollTo: function(top) {
        if (top == undefined) var top = window.scrollY;
        var _obj = {
            top: window.scrollY
        };
        var extra = App.isMobile ? 20 : 100;
        TweenLite.to(_obj, 0.75, {
            ease: Power2.easeOut,
            top: top - extra,
            onUpdate: function() {
                window.scroll(0, _obj.top);
            }
        });
    }
}

function isMobileDevice() {
    return (typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1);
};

function showItem(item, delay) {
    TweenLite.to(item, 0.5, {
        ease: Power2.easeOut,
        delay: delay,
        opacity: 1,
        y: 0
    });
}

$(document).ready(App.init());
$(window).on("load", App.load());
