<meta name="title" content="Genomma Lab Peru"/>
<meta name="author" content="@genommalab"/>
<meta name="description" content="Genomma Lab Peru: Somos uma multinacional presente em mais de 15 países, que se destaca, dentre vários pontos, pelo sucesso de nossas marcas e campanhas publicitárias com alto investimento em mídia."/>
<meta property="og:url" content=""/>
<meta property="og:locale" content="es_LA"/>
<meta property="og:type" content="website"/>
<meta property="og:title" content="Genomma Lab"/>
<meta property="og:description" content="Genomma Lab Peru: Somos uma multinacional presente em mais de 15 países, que se destaca, dentre vários pontos, pelo sucesso de nossas marcas e campanhas publicitárias com alto investimento em mídia."/>
<meta property="og:image" content="/dist/img/share-image.png"/>
<meta name="twitter:card" content="summary_large_image"/>
<meta name="twitter:site" content="@genommalab"/>
<meta name="twitter:creator" content="@genommalab"/>
<meta name="twitter:title" content="Genomma Lab"/>
<meta name="twitter:description" content="Genomma Lab Peru: Somos uma multinacional presente em mais de 15 países, que se destaca, dentre vários pontos, pelo sucesso de nossas marcas e campanhas publicitárias com alto investimento em mídia."/>
<meta name="twitter:image" content="/dist/img/share-image.png"/>
