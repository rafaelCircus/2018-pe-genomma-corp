<link rel="apple-touch-icon" sizes="180x180" href="/dist/favicons/apple-touch-icon.png">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-title" content="Asepxia">
<link rel="manifest" href="/dist/favicons/manifest.json">
<meta name="mobile-web-app-capable" content="yes">
<meta name="theme-color" content="#060856">
<meta name="application-name" content="Asepxia">
<meta name="msapplication-TileColor" content="#060856">
<meta name="msapplication-TileImage" content="/dist/favicons/mstile-150x150.png">
<meta name="msapplication-config" content="/dist/favicons/browserconfig.xml">
<link rel="icon" type="image/png" sizes="32x32" href="/dist/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="/dist/favicons/favicon-16x16.png">
<link rel="shortcut icon" href="/dist/favicons/favicon.ico">
