@extends('layout')
@section('content')
	<div class="products col-md-6">
		<h1>Lista de productos</h1>
		<ul>
			@foreach($products as $product)
				<li>
					<a href="/producto/{{ $product->id }}" title="{{ $product->name }}">
						{{ $product->name }}
					</a>
				</li>
			@endforeach
		</ul>
	</div>
@endsection
