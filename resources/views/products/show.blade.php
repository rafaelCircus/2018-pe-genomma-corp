@extends('layout')
@section('content')
	<div class="product col-md-6">
		<h2>{{ $product->name }}</h2>
		<p>{{ $product->description }}</p>
	</div>
@endsection