<div class="section" id="about-genomma">
  <div class="container item-animable">
    <h2 class="section-title">Sobre genomma</h2>
    <div class="row mobile_slider">
      <div class="col-md-4 section-block">
        <h3 class="section-subtitle">Nuestra misión</h3>
        <p>
          Mejorar y preservar la salud y bienestar de las personas mediante productos innovadores, seguros y eficaces, otorgando oportunidades de desarrollo a nuestros colaboradores y rentabilidad a nuestros accionistas e impactando positivamente a la comunidad y al medio ambiente.
        </p>
        {{-- <a href="#" class="btn">VER MÁS</a> --}}
      </div>
      <div class="col-md-4 section-block">
        <h3 class="section-subtitle">Nuestra visión</h3>
        <p>
          Ser la empresa líder en nuestras categorías de medicamentos y productos para el cuidado personal, y ser reconocidos por impactar positivamente en la salud y bienestar de las personas, comunidad y medio ambiente.
        </p>
        {{-- <a href="#" class="btn">VER MÁS</a> --}}
      </div>
      <div class="col-md-4 section-block">
        <h3 class="section-subtitle">Nuestros valores</h3>
        <ul class="dots">
          <li><i></i>Integridad</li>
          <li><i></i>Inovación / Creatividad</li>
          <li><i></i>Trabajo en equipo</li>
          <li><i></i>Sustentabilidad</li>
          <li><i></i>Eficiencia / Eficacia</li>
        </ul>
        {{-- <a href="#" class="btn">VER MÁS</a> --}}
      </div>
    </div>
  </div>
</div>
