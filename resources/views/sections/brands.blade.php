<div class="section no-padding">
  <div class="container item-animable" id="brands">


      <div class="pleca-top"></div>
      <h2 class="section-title">Marcas</h2>
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          {{-- <ul>
            <li><a href="">TODAS</a></li>
            <li><a href="">BIENESTAR</a></li>
            <li><a href="">DERMA</a></li>
            <li><a href="">FARMA</a></li>
          </ul> --}}
        </div>
      </div>

    <div class="row item-animable">
      <div class="col-md-10 col-md-offset-1">
        <div class="row">
          <div class="col-md-3 col-xs-4">
            <a href="https://asepxia.com/ec/" target="_blank" class="logo-hover asepxia">
              <img src="/dist/img/brands/tranparent.png" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-4">
            <span class="logo-hover cicatricure">
              <img src="/dist/img/brands/tranparent.png" alt="">
            </span>
            {{-- <a href="https://www.cicatricure.com.ec/" target="_blank" class="logo-hover cicatricure"> --}}
              {{-- <img src="/dist/img/brands/tranparent.png" alt=""> --}}
            {{-- </a> --}}
          </div>
          <div class="col-md-3 col-xs-4">
            <span class="logo-hover tio-nacho">
              <img src="/dist/img/brands/tranparent.png" alt="">
            </span>
            {{-- <a href="https://tionacho.com.br/" target="_blank" class="logo-hover tio-nacho">
              <img src="/dist/img/brands/tranparent.png" alt="">
            </a> --}}
          </div>
          <div class="col-md-3 col-xs-4">
            <a href="https://medicasp.com.pe/" target="_blank" class="logo-hover medicasp">
              <img src="/dist/img/brands/tranparent.png" alt="">
            </a>
          </div>
          <div class="col-md-3 col-xs-4">
            <a href="https://nikzon.com.pe/" target="_blank" class="logo-hover nikzon">
              <img src="/dist/img/brands/tranparent.png" alt="">
            </a>
          </div>
        </div>
      </div>
    </div>

    <div class="pleca-bottom"></div>

  </div>
</div>
