<div class="section blue" id="work-on">
  <div class="container item-animable">
    <h2 class="section-title">TRABAJE CON NOSOTROS</h2>
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        {{-- <p>
          <a href="mailto:cintia.huaman@genommalab.com" target="_blank" class="btn">MANDAR CV</a>
        </p> --}}
        <p class="text-center">
          <a href="mailto:madeleine.orihuela@genommalab.com" class="btn" style="display: inline-block">Enviar CV</a>
        </p>
      </div>
    </div>
  </div>
</div>
