<div class="section item-animable" id="whats-new" style="padding: 0px 0 110px;">
  <div class="container">
    <h2 class="section-title">NOVEDADES</h2>
    <div class="row mobile_slider">

      <div class="col-md-5 col-md-offset-1 section-block">
        <div class="row">
          <div class="col-md-4 side_image">
            <img src="/dist/img/genommalab-parches-asepxia.png" alt="parches-asepxia">
          </div>
          <div class="col-md-8">
            <h3 class="section-subtitle">CARBÓN DETOX PURIFICANTE</h3>
            <p>
              <strong>Asepxia presenta su nueva Línea Carbón Detox Purificante</strong>
            </p>
            <p>
              Su fórmula con carbón détox ayuda a combatir la suciedad causada por la polución.<br>
              •HIPOALERGÉNICO<br>
              •Enriquecida con minerales<br>
              •Remueve toxinas y bacterias de la piel.<br>
              •Alto poder de absorción de oleosidad<br>
            </p>
          </div>
        </div>
      </div>

      <div class="col-md-5  section-block">
        <div class="row">
          <div class="col-md-4 side_image">
            <img src="/dist/img/genomma-crema.png" alt="" class="right">
          </div>
          <div class="col-md-8 section-block__image">
            <h3 class="section-subtitle">Crema Contorno de Ojos Blur & Filler</h3>
            <p>
              Cicatricure lanza su nueva fórmula de Crema Contorno de Ojos Blur & Filler con Biopeptide Regen, junto con nuevos talentos peruanos para cada una de sus líneas.
            </p>
            {{-- <a href="#" class="btn">VER MÁS</a> --}}
          </div>
        </div>
      </div>

    </div>
  </div>
</div>
