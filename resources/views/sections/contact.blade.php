<div class="section contact-bg">
  <div class="container item-animable" id="contact-us">
    <h2 class="section-title">CONTACTO</h2>
    <div class="row">
        <div class="col-md-4 col-md-offset-1">
          <ul class="address">
            {{-- <li><strong>Emai:</strong> <a href="mailto:sac@genommalab.com.br" class="link">sac@genommalab.com.br</a></li> --}}
            <li><strong>Teléfono:</strong> <a href="tel:0800-00-791" class="link">0800-00-791</a></li>
            <li><strong>Dirección:</strong> <a href="https://goo.gl/maps/4dPoomoWoC32" target="_blank" class="link">Calle Esquilache 39, San <br>Isidro 15073, Perú</a></li>
          </ul>
        </div>
        <div class="col-md-5" id="maps">
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d527.7967814681972!2d-77.03847088092144!3d-12.10404692629564!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9105c841479c7d51%3A0x55dbc8b750556d62!2sCalle+Esquilache+39%2C+San+Isidro+15073%2C+Per%C3%BA!5e0!3m2!1ses!2smx!4v1531785613663" frameborder="0" style="border:0"></iframe>
        </div>
   </div>
  </div>
</div>
