<div class="section no-padding" id="about-us">
  <div class="container left-blue item-animable only-fade">
    <h2 class="section-title border-left item-animable">GENOMMA LAB PERÚ</h2>
     <div class="col-md-8 col-md-offset-2">
       <p>
        <strong>Somos una empresa joven, dinámica e innovadora;</strong>
        ocupados y preocupados para encontrar <strong>soluciones para mejorar la calidad de vida y la salud </strong>de
        todos aquellos que se benefician con el uso apropiado de nuestros
        productos.
      </p>
      <p>
        Somos una extensa red que agrupa a empresas e instituciones que investigan,
        fabrican y comercializan productos farmacéuticos y dermo-cosméticos que
        cumplen con los más altos estándares de calidad, apegados al cumplimiento
        de la normatividad nacional: las buenas prácticas de fabricación, de
        documentación y analíticas.
      </p>
      <p>
        En la industria farmacéutica algún día todo será posible y es por eso que
        hoy estamos construyendo un futuro.
      </p>
      <p>
        Un futuro que es la cristalización de los sueños de sus fundadores y que se
        ha podido lograr gracias a la creatividad, capacidad, esfuerzo y tenacidad
        de todos aquellos que están involucrados en el proyecto de hacer que día a
        día Genomma Lab sea la farmacéutica mexicana de orgullo.
      </p>
      <p>
        Siendo la única farmacéutica mexicana cotizando en la Bolsa Mexicana de
        Valores, se ha creado nuestro <a href="http://www.genommalab.com/Inversionistas/" class="link" title="Portal de Inversionistas" target="_blank">Portal de Inversionistas</a> en donde se pueden
        conocer los resultados de nuestra empresa y los avances que se tienen cada
        trimestre en ventas.
      </p>
    </div>
    <div class="pleca-bottom"></div>
  </div>
</div>
