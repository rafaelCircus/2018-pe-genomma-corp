<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<ul>
					<li>2018 GENOMMA LAB <span class="mobile-hide">-</span> TODOS LOS DERECHOS RESERVADOS</li>
				</ul>
			</div>
			<div class="col-md-6">
				<ul>
					<li><a href="/politicas" target="_blank">POLÍTICAS DE PRIVACIDAD</a></li>
					<li><a href="/avisoLegal" target="_blank">AVISO LEGAL</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
