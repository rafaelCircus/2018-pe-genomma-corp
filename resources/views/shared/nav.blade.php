<nav class="nav-container edge-bottom" id="main-nav-container">
	<div class="container">
		<button id="toggle-menu" class="toggle-nav toggle toggle-class" data-target="#main-nav-container">
			<span class="toggle-icon"></span>
		</button>
		<a href="/" class="home-logo">
			<img src="dist/img/genomalab-logo_pe.svg
			" alt="">
		</a>
		<div id="main-nav-content" class="nav-content col-md-12">
			<ul>
				<li id="change-location-mobile">
			      <a href="#" title="Cambiar de país" class="countryM">País</a>
						<ul id="country-selectorM">
							<li><a href="http://genommalab.com/ar" title="Argentina">Argentina</a></li>
							<li><a href="http://genommalab.com/br" title="Brasil"></i> Brasil</a></li>
							<li><a href="http://genommalab.com/cl" title="Chile">Chile</a></li>
							<li><a href="http://genommalab.com/co" title="Colombia">Colombia</a></li>
							<li><a href="http://genommalab.com/ec" title="Ecuador"></i> Ecuador</a></li>
							<li><a href="http://genommalab.com/mx" title="México">México</a></li>
							<li><a href="http://genommalab.com/pe" title="Perú">Perú</a></li>
							<li><a href="http://www.genommalab.us" title="Usa">Usa</a></li>
							{{-- <li><a href="http://esr.genommalab.com/" title="Sustentabilidad">Sustentabilidad</a></li> --}}
						</ul>
				</li>

				<li><a href="#about-us" class="menu-link hoverable">Genomma Lab</a></li>
				<li><a href="#brands" class="menu-link hoverable">Marcas</a></li>
				<li><a href="#whats-new" class="menu-link hoverable">NOVEDADES</a></li>
				<li><a href="#work-on" class="menu-link hoverable">TRABAJE CON NOSOTROS</a></li>
				<li><a href="#contact-us" class="menu-link hoverable">CONTACTO</a></li>
			</ul>
		</div>

		{{-- desktop --}}
		<div id="change-location" class="">
			<a href="#" title="Cambiar de país" class="country">País</a>
			<ul id="country-selector">
				<li><a href="http://genommalab.com/ar" title="Argentina">Argentina</a></li>
				<li><a href="http://genommalab.com/br" title="Brasil"></i> Brasil</a></li>
				<li><a href="http://genommalab.com/cl" title="Chile">Chile</a></li>
				<li><a href="http://genommalab.com/co" title="Colombia">Colombia</a></li>
				<li><a href="http://genommalab.com/ec" title="Ecuador"></i> Ecuador</a></li>
				<li><a href="http://genommalab.com/mx" title="México">México</a></li>
				<li><a href="http://genommalab.com/pe" title="Perú">Perú</a></li>
				<li><a href="http://www.genommalab.us" title="Usa">Usa</a></li>
				{{-- <li><a href="http://esr.genommalab.com/" title="Sustentabilidad">Sustentabilidad</a></li> --}}
			</ul>
		</div>
		{{-- <section id="lang-selector-container">
			<ul>
				<li><a href="/es" class="menu-link active">ESP</a></li>
				<li><a href="/en" class="menu-link ">ENG</a></li>
			</ul>
		</section> --}}
	</div>
</nav>
