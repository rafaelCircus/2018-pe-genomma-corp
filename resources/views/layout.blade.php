<!DOCTYPE html>
<html class="no-js">
  <head>
    {{-- google analytics script --}}
    @include("snippets.analytics")
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no, user-scalable=no">
    <title>Genomma Lab Perú @yield("title")</title>
    {{-- metatags --}}
    @include("snippets.metatags")
    {{-- favicons --}}
    @include("snippets.favicons")
    {{-- stylesheets --}}
    <link rel="stylesheet" href="/dist/css/main.css">
    @yield("styles")
  </head>
  <body>
    {{-- google tag manager --}}
    @include("snippets.tag_manager")
    @include("shared.shim")

    @include("shared.preloader")
    @include("shared.nav")

    <div id="main-wrapper">
      {{-- content --}}
      @yield('content')
    </div>

    {{-- footer --}}
    @include("shared.footer")
    {{-- scripts --}}
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="/dist/js/main.js"></script>
  </body>
</html>
