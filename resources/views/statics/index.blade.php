@extends('layout')
@section('content')
	{{-- @include('home.imagen') --}}

	@include('sections.main_slider')

	<div class="sections-wrapper">
		@include('sections.main')

		@include('sections.banner')

		@include('sections.brands')

		@include('sections.three_cols')

	</div>

	@include('sections.two_cols')
	@include('sections.single_col_bottom')

	@include('sections.contact')

@endsection

@section('styles')
	<style media="screen">
		.main-image{
		  background-image: url(dist/img/home-main.png);
		}
	</style>
@endsection
