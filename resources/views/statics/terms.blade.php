@extends('legal')
@section('content')

	<div class="sections-wrapper">

		<div class="section">
      <div class="container left-blue item-animable only-fade">
          <h2 class="section-title item-animable">Aviso legal</h2>

            <div class="col-md-8 col-md-offset-2">
							<p>
									El presente aviso establece los fines, ámbito y propósito de la información
									contenida en nuestra página web, la cual se proporciona para promover el
									derecho de acceso a la información al público respecto a nuestros
									productos, servicios y cualquier otra comunicación vinculada con los mismos
									que pueda considerarse como de interés general, lo anterior con la
									finalidad de fomentar una vida saludable a manera de prevención de
									problemas físicos y de salud.
							</p>
							<p>
									Las información publicada en nuestra página web únicamente debe
									considerarse como material de apoyo documental, siendo responsable la
									fuente citada, por lo que Genommma Lab Internacional S.A.B. de C.V.,
									filiales, asociadas, licenciantes, licenciatarias y/o partes relacionadas
									(en adelante Genomma Lab) no son responsables del uso o las acciones
									derivadas por la comunicación de la información aquí contenida.
							</p>
							<p>
									La información publicada en la presente página web respecto a padecimientos
									que afectan la salud no debe ser considerada como diagnóstico final u
									opinión profesional respecto a algún malestar o padecimiento. Se recomienda
									que cualquier padecimiento sea atendido directamente por un especialista de
									la salud.
							</p>
							<p>
									En caso de que cualquier persona use la información publicada como
									diagnóstico final o como opinión profesional, Genomma Lab se deslinda de
									cualquier responsabilidad en relación a cualquier tercero que aplique o use
									dicha información. Toda persona relacionada con la actualización, diseño y
									redacción de la presente página web, se reserva el derecho de hacer las
									modificaciones y/o actualizaciones pertinentes a la página web sin previo
									aviso, excluyéndose de cualquier responsabilidad que conlleve dicha
									actualización.
							</p>
							<p>
									La información comunicada en la página web puede contener enlaces a páginas
									externas. La información presentada en dichas páginas de terceros es
									responsabilidad de los titulares de dichas páginas web. Genomma Lab, se
									deslinda de cualquier responsabilidad de la información publicada en
									nombres de dominio y/o páginas web de terceros.
							</p>
							<p>
									Se recomienda leer atentamente nuestra Política de Privacidad en su
									totalidad antes de usar Nuestra página Web o cualquiera de nuestros
									servicios vía Internet, ya que estos regulan nuestro tratamiento de
									cualquier información, incluidos los datos personales que nos proporciones.
									Todos los usuarios deben considerar que cierta información, declaraciones,
									datos y contenido podrían, o muy probablemente pueden, revelar su sexo,
									origen étnico, nacionalidad, edad u otros datos personales.
							</p>
							<p>
									En este acto por el hecho de usar y navegar en nuestro nombre de dominio
									los usuarios manifiestan su consentimiento a efecto de que cualquier
									información, declaraciones, datos o contenidos que proporcionen algún
									formulario o en general de manera escrita facilite se otorgan de forma
									voluntaria y que Genomma Lab podrá tratar dicha información de acuerdo con
									los términos de la Política de Privacidad.
							</p>
            </div>

      </div>
    </div>

		{{-- footer --}}
    @include("shared.footer")

	</div>

@endsection

@section('styles')
	<style media="screen">
		.main-image{
		  background-image: url(dist/img/home-main.png);
		}
	</style>
@endsection
