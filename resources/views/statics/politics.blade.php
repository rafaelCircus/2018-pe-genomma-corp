@extends('legal')
@section('content')

	<div class="sections-wrapper">

    <div class="section">
      <div class="container left-blue item-animable only-fade">
          <h2 class="section-title item-animable">Aviso de Privacidad</h2>

            <div class="col-md-8 col-md-offset-2">
							<p>
									Genomma Lab Internacional, S.A.B. de C.V. (en adelante "Genomma"), con
									domicilio en Avenida Antonio Dovalí Jaime Número 70, Torre C, Piso 2,
									Despacho "A", Colonia Santa Fé, C.P. 01210, Delegación Alvaro Obregón,
									Ciudad de México, México, en estricto cumplimiento a lo dispuesto en la Ley
									Federal de Protección de Datos Personales en Posesión de los Particulares y
									su reglamento (en adelante la "Ley"), así como de cualquier otro
									ordenamiento legal análogo en cualquier parte del mundo, hace de su
									conocimiento, que los datos personales (en adelante "Datos Personales") que
									a través de nuestros diversos servicios podría proporcionarnos, serán
									utilizados para la realización de actividades comerciales como: (i)
									seguimiento y evaluación de la prestación de bienes y/o servicios que
									suministramos; (ii) cotización de bienes y/o servicios cuyo suministro
									solicitamos; (iii) atención de dudas y sugerencias; (iv) pago y cobro de
									contraprestaciones y facturación; (v) análisis y elaboración de
									estadísticas o reportes; (vi) elaboración de contratos derivados de la
									relación comercial; (vii) comercialización de productos de Genomma (viii).
							</p>
							<p>
									Los datos que podríamos solicitar no se consideran datos sensibles en
									términos de la Ley, estos podrán referir a: Nombre, Domicilio, Número
									telefónico fijo, Número de teléfono Móvil, Correo Electrónico, Vinculación
									a las cuentas de redes sociales enunciativamente Facebook, Instagram o
									cualquier otra de naturaleza análoga.
							</p>
							<p>
									Al proporcionar Datos Personales a Genomma, usted confirma que está de
									acuerdo con los términos de nuestro aviso de privacidad y consiente el
									tratamiento que Genomma realizará para fines comerciales, mercadológicos y
									demás aquí descritos.
							</p>
							<p>
									Si no está de acuerdo con cualquier término de este aviso de privacidad,
									por favor no proporcione ningún tipo de Datos Personales.
							</p>
							<p>
									Los Datos Personales que usted proporcione a Genomma, serán tratados de
									manera lícita y observando en todo momento los principios de
									consentimiento, información, calidad, finalidad, lealtad, proporcionalidad
									y responsabilidad en cumplimiento con lo establecido por la Ley.
							</p>
							<p>
									Los Datos Personales que nos proporcione se resguardarán en bases de datos
									con acceso limitado, de conformidad con la Ley.
							</p>
							<p>
									Genomma no vende ni comunica o divulga los datos personales de sus usuario,
									no obstante lo anterior podremos compartir los datos de sus usuarios con
									sus filiales, de igual forma Genomma podrá compartir los datos personales
									de sus usuarios: (I) Si la ley o un proceso legal así lo exige; (II) Ante
									las autoridades competentes u otras autoridades gubernamentales lo
									solicitan o (III) Cuando Genomma considere que la publicación de los mismos
									es necesaria o apropiada para evitar daños físicos o pérdidas económicas o
									en relación con una investigación sobre alguna actividad ilegal.
							</p>
							<p>
									En el supuesto de que Genomma pretenda una finalidad o uso distinto al
									señalado en el presente aviso, y que el mismo no sea compatible o análogo
									con las que se decriben en el mismo, se informará al titular de los datos
									personales, según se haya manifestado, para en su caso solicitar su
									consentimiento para el ejercicio de la nueva finalidad.
							</p>
							<p>
									Usamos cookies y tecnologías similares, incluidos identificadores de
									dispositivos móviles, para ayudarnos a reconocerte, mejorar tu experiencia
									en nuestra página web, aumentar la seguridad, medir el uso de nuestros
									servicios y proporcionar publicidad. Puedes controlar las cookies a través
									de la configuración de tu navegador y de otras herramientas. Al hacer uso
									de nuestros servicios en la página web otorgas tu consentimiento para que
									se coloquen cookies en tu navegador en virtud de esta Política de
									privacidad.
							</p>
							<p>
									Respecto de los Datos Personales que se encuentren en posesión de Genomma
									usted tiene derecho de (i) acceder a ellos y a los detalles del tratamiento
									de los mismos, (ii) rectificarlos en caso de ser inexactos o incompletos,
									(iii) cancelarlos cuando considere que no se requieren para alguna de las
									finalidades señaladas en el presente aviso de privacidad, estén siendo
									utilizados para finalidades no consentidas o haya finalizado la relación
									contractual o de servicio, o bien, (iv) oponerse al tratamiento de los
									mismos para fines específicos, presentando una solicitud al correo
									electrónico datospersonales@genommalab.com (en adelante los "Derechos
									ARCO").
							</p>
							<p>
									Toda solicitud para limitar o revocar el uso o divulgación de sus Datos
									Personales y en general para ejercer sus Derechos ARCO, deberá contener (i)
									nombre y dirección de correo electrónico para comunicarle la respuesta a su
									solicitud, (ii) documentos que acrediten la identidad o, en su caso, su
									representación legal, (iii) la descripción clara y precisa de los Datos
									Personales respecto de los que se busca ejercer alguno de los derechos y
									(iv) cualquier otro elemento que facilite la localización de los Datos
									Personales.
							</p>
							<p>
									Nos reservamos el derecho de efectuar en cualquier momento modificaciones o
									actualizaciones al presente aviso de privacidad, estas modificaciones
									estarán anunciadas y disponibles en este mismo medio.
							</p>
            </div>

      </div>
    </div>

		{{-- footer --}}
    @include("shared.footer")

	</div>

@endsection
